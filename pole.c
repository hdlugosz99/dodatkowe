#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fun.h"

float f_pole(float p_promien){
    float pole = M_PI * p_promien * p_promien;
    return pole;
}