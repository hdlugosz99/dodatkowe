CC=gcc
CFLAGS=-Wall 
LIBS=-lm

dodatkowe: dodatkowe.o obwod.o pole.o
	$(CC) $(CFLAGS) -o dodatkowe dodatkowe.o obwod.o pole.o $(LIBS)

dodatkowe.o: dodatkowe.c fun.h
	$(CC) $(CFLAGS) -c dodatkowe.c

obwod.o: obwod.c
	$(CC) $(CFLAGS) -c obwod.c

pole.o: pole.c
	$(CC) $(CFLAGS) -c pole.c


	