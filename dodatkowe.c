#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "fun.h"

int main()
{
    printf("Program wyliczajacy obwod oraz pole kola.\n");
    printf("Prosze podac promien: ");

    float promien = 0.0;

    do{
        scanf("%f", &promien);
    }while(promien <= 0);

    printf("Obwod kola wynosi: %.4f\n", f_obwod(promien));
    printf("Pole kola wynosi: %.4f\n", f_pole(promien));

    getch();

    return 0;
}

